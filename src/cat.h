/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#ifndef CAT_H
#define CAT_H   1

#include "common.h"
#include "ifft.h"
#include <termios.h>
#include <fcntl.h>
#include <sys/ioctl.h>

/* Transceiver type */
enum
{
  NONE = 0,
  FT847,
  FT857,
  K2,
  K3,
  PERSEUS
};

#define MAX_SERIAL_RETRIES  10

/* Range (+/-) in pixels to look for IFFT maximum
 * when click-to-tune on waterfall by user */
#define CLICK_TUNE_RANGE    20

#endif


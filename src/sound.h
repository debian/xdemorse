/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#ifndef SOUND_H
#define SOUND_H 1

#include <alsa/asoundlib.h>
#include "common.h"

#define SND_PERIOD_SIZE     4096 /* PCM period size */
#define SND_BUF_STRIDE      8192 /* Sound buffer stride. Must be 2 * PERIOD_SIZE */
#define SND_READI_FRAMES    512  /* Number of frames to read from dsp */
#define SND_NUM_PERIODS     4    /* Number of periods */
#define SND_EXACT_VAL       0    /* Set excat value of hwparams */
#define SND_NUM_CHANNELS    2    /* 1 = MONO, 2 = STEREO <- only accepted */
#define SND_OPEN_CAPTURE    1    /* Open PCM device for capture  */
#define SND_OPEN_PLAYBACK   2    /* Open PCM device for playback */
#define PLAYBACK_BUFFS      4    /* Number of playback buffers */

#endif


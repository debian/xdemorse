/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include "main.h"
#include "shared.h"
#include <stdlib.h>

/* Signal handler */
static void sig_handler( int signal );

/*------------------------------------------------------------------------*/

  int
main (int argc, char *argv[])
{
  /* Main window */
  GtkWidget *main_window;

  /* Command line option returned by getopt() */
  int option;

  /* New and old actions for sigaction() */
  struct sigaction sa_new, sa_old;

  /* Initialize new actions */
  sa_new.sa_handler = sig_handler;
  sigemptyset( &sa_new.sa_mask );
  sa_new.sa_flags = 0;

  /* Register function to handle signals */
  sigaction( SIGINT,  &sa_new, &sa_old );
  sigaction( SIGSEGV, &sa_new, 0 );
  sigaction( SIGFPE,  &sa_new, 0 );
  sigaction( SIGTERM, &sa_new, 0 );
  sigaction( SIGABRT, &sa_new, 0 );

  gtk_init (&argc, &argv);

  /* Process command line options */
  while( (option = getopt(argc, argv, "hv") ) != -1 )
    switch( option )
    {
      case 'h': /* Print usage and exit */
        Usage();
        exit( 0 );

      case 'v': /* Print version */
        puts( PACKAGE_STRING );
        exit( 0 );

      default: /* Print usage and exit */
        Usage();
        exit( -1 );

    } /* End of switch( option ) */

  /* Create the file path to xdemorse Glade file */
  Strlcpy( rc_data.xdemorse_glade, getenv("HOME"), sizeof(rc_data.xdemorse_glade) );
  Strlcat( rc_data.xdemorse_glade,
      "/.xdemorse/xdemorse.glade", sizeof(rc_data.xdemorse_glade) );

  /* Check for the Glade config file */
  FILE *fp = fopen( rc_data.xdemorse_glade, "r" );
  if( fp == NULL )
  {
    fprintf( stderr, "xdemorse: cannot open xdemorse Glade GUI Description file.\n" );
    perror( rc_data.xdemorse_glade );
    fprintf( stderr, "xdemorse: trying to create xdemorse config directory "
                     "from the installation prefix file tree.\n" );

    /* Find the binary's path (location in file system) */
    char exe_path[256], file_path[288];
    
    /* Read the file path to xdemorse executable */
    size_t len = sizeof( exe_path );
    int bytes = (int)readlink( "/proc/self/exe", exe_path, len );
    if( bytes <= 0 )
    {
      fprintf( stderr, "xdemorse: cannot read xdemorse binary's location.\n" );
      perror( "/proc/self/exe" );
      exit( -1 );
    }

    /* Remove "/bin/xdemorse" from the path with room for termination */
    bytes -= sizeof( "/bin/xdemorse" ) - 1;
    if( bytes < 1 )
    {
      fprintf( stderr, "xdemorse: cannot create file path to examples/xdemorse.\n" );
      exit( -1 );
    }

    /* Create file path to xdemorse examples directory */
    exe_path[bytes] = '\0';
    Strlcpy( file_path, exe_path, sizeof(file_path) );
    Strlcat( file_path, "/share/examples/xdemorse", sizeof(file_path) );
    fprintf( stderr, "xdemorse: creating xdemorse config directory from: %s\n", file_path );

    /* Create system command to copy examples/xdemorse to ~/.xdemorse */
    char syscmnd[512];
    Strlcpy( syscmnd, "cp -r ", sizeof(syscmnd) );
    Strlcat( syscmnd, file_path, sizeof(syscmnd) );
    Strlcat( syscmnd, " ", sizeof(syscmnd) );
    Strlcat( syscmnd, getenv("HOME"), sizeof(syscmnd) );
    Strlcat( syscmnd, "/.xdemorse",   sizeof(syscmnd) );
    int ret = system( syscmnd );
    if( ret == -1 )
    {
      fprintf( stderr,"xdemorse: cannot create xdemorse's working directory.\n" );
      perror( file_path );
      exit( -1 );
    }
  } /* if( fp == NULL ) */
  else fclose( fp );

  /* Create xdemorse main window */
  main_window = create_main_window( &main_builder );
  gtk_window_set_title( GTK_WINDOW(main_window), (const gchar *)PACKAGE_STRING );

  /* Get Rx text buffer and scroller */
  rx_text_buffer = gtk_text_view_get_buffer
    ( GTK_TEXT_VIEW(Builder_Get_Object(main_builder, "rx_textview")) );
  rx_scrolledwindow = Builder_Get_Object( main_builder, "rx_scrolledwindow" );

  /* Get widgets and spin buttons */
  scope       = Builder_Get_Object( main_builder, "scope_drawingarea" );
  waterfall   = Builder_Get_Object( main_builder, "waterfall_drawingarea" );
  scope_label = Builder_Get_Object( main_builder, "scope_label" );
  receiver_combobox = GTK_COMBO_BOX_TEXT(
      Builder_Get_Object(main_builder, "receiver_combobox") );
  frequency   = GTK_SPIN_BUTTON( Builder_Get_Object(main_builder, "frequency_spinbutton") );
  speed       = GTK_SPIN_BUTTON( Builder_Get_Object(main_builder, "wpm_spinbutton") );
  squelch     = GTK_SPIN_BUTTON( Builder_Get_Object(main_builder, "squelch_spinbutton") );
  ratio       = GTK_SPIN_BUTTON( Builder_Get_Object(main_builder, "ratio_spinbutton") );

  rc_data.det_squelch = (int)gtk_spin_button_get_value_as_int( squelch );
  rc_data.det_ratio   = gtk_spin_button_get_value( ratio );
  rc_data.speed_wpm   = (int)gtk_spin_button_get_value_as_int( speed );
  Set_Flag( SELECT_LEVEL );
  Set_Flag( DISPLAY_SIGNAL );
  rc_data.unit_elem = 15;
  rc_data.receiver_freq = gtk_spin_button_get_value_as_int( frequency );

#ifdef HAVE_LIBPERSEUS_SDR
  gtk_combo_box_text_append_text( receiver_combobox, "PERSEUS" );
#endif

  gtk_widget_show (main_window);

  /* Load runtime config file, abort on error */
  g_idle_add( Load_Config, NULL );

  gtk_main ();

  return 0;
}

/*------------------------------------------------------------------------*/

/*  Usage()
 *
 *  Prints usage information
 */

  void
Usage( void )
{
  fprintf( stderr, "%s\n",
      _("Usage: xdemorse [-hv]") );

  fprintf( stderr, "%s\n",
      _("       -h: Print this usage information and exit"));

  fprintf( stderr, "%s\n",
      _("       -v: Print version number and exit"));

} /* End of Usage() */

/*------------------------------------------------------------------------*/

/*  sig_handler()
 *
 *  Signal Action Handler function
 */

static void sig_handler( int signal )
{
  /* Wrap up and quit */
  Cleanup();
  fprintf( stderr, "\n" );
  switch( signal )
  {
    case SIGINT :
      fprintf( stderr, "%s\n",  _("xdemorse: Exiting via User Interrupt") );
      exit( signal );

    case SIGSEGV :
      fprintf( stderr, "%s\n",  _("xdemorse: Segmentation Fault") );
      exit( signal );

    case SIGFPE :
      fprintf( stderr, "%s\n",  _("xdemorse: Floating Point Exception") );
      exit( signal );

    case SIGABRT :
      fprintf( stderr, "%s\n",  _("xdemorse: Abort Signal received") );
      exit( signal );

    case SIGTERM :
      fprintf( stderr, "%s\n",  _("xdemorse: Termination Request received") );
      exit( signal );
  }

} /* End of sig_handler() */

/*------------------------------------------------------------------------*/


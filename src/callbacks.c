/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include "callbacks.h"
#include "shared.h"

/* Pop up error dialog */
static GtkWidget *error_dialog = NULL;

/*------------------------------------------------------------------------*/

/*  Error_Dialog()
 *
 *  Opens an error dialog box
 */

  void
Error_Dialog( char *message )
{
  GtkWidget *label;
  GtkBuilder *builder;

  if( !error_dialog )
  {
    error_dialog = create_error_dialog( &builder );
    label = Builder_Get_Object( builder, "error_message" );
    gtk_label_set_text( GTK_LABEL(label), (const gchar *)message );
    gtk_widget_show( error_dialog );
    g_object_unref( builder );
  }
}


  void
on_main_window_destroy(
    GObject *object,
    gpointer user_data)
{
  gtk_main_quit();
}


  gboolean
on_main_window_delete_event(
    GtkWidget *widget,
    GdkEvent  *event,
    gpointer   user_data)
{
  Cleanup();
  g_object_unref( main_builder );
  return( FALSE );
}


  gboolean
on_scope_drawingarea_draw(
    GtkWidget *widget,
    cairo_t   *cr,
    gpointer  user_data)
{
  Display_Scope( cr );
  return( TRUE );
}

  void
on_receiver_combobox_changed(
    GtkComboBox     *combobox,
    gpointer         user_data)
{
  GtkWidget *toggle = Builder_Get_Object( main_builder, "receive_togglebutton" );
  gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON(toggle), FALSE );

  gchar *txt = gtk_combo_box_text_get_active_text( receiver_combobox );
  if( strcmp(txt, "FT847") == 0 )
    rc_data.receiver_type = FT847;
  else if( strcmp(txt, "FT857") == 0 )
    rc_data.receiver_type = FT857;
  else if( strcmp(txt, "K2") == 0 )
    rc_data.receiver_type = K2;
  else if( strcmp(txt, "K3") == 0 )
    rc_data.receiver_type = K3;
  else if( strcmp(txt, "PERSEUS") == 0 )
    rc_data.receiver_type = PERSEUS;
  else if( strcmp(txt, "-- NONE --") == 0 )
    rc_data.receiver_type = NONE;

  if( rc_data.receiver_type == PERSEUS )
    rc_data.open_type = SND_OPEN_PLAYBACK;
  else
    rc_data.open_type = SND_OPEN_CAPTURE;

  /* Enable Transceiver CAT */
  if( rc_data.receiver_type == NONE )
    Clear_Flag( ENABLE_CAT );
  else
    Set_Flag( ENABLE_CAT );
}


  void
on_frequency_spinbutton_value_changed(
    GtkSpinButton *spin_button,
    gpointer       user_data)
{
  rc_data.receiver_freq = gtk_spin_button_get_value_as_int( spin_button );
  Write_Rx_Freq( rc_data.receiver_freq );
}


  gboolean
on_frequency_spinbutton_scroll_event(
    GtkWidget *widget,
    GdkEvent  *event,
    gpointer   user_data)
{
  if( event->scroll.direction == GDK_SCROLL_UP )
    rc_data.receiver_freq++;
  else if( event->scroll.direction == GDK_SCROLL_DOWN )
    rc_data.receiver_freq--;
  gtk_spin_button_set_value( frequency, (double)rc_data.receiver_freq );
  return( TRUE );
}


  void
on_wpm_spinbutton_value_changed(
    GtkSpinButton *spin_button,
    gpointer       user_data)
{
  int val = gtk_spin_button_get_value_as_int( spin_button );
  rc_data.unit_elem = (60 *rc_data.tone_freq) / (50 * val * CYCLES_PER_FRAG);
}


  void
on_squelch_spinbutton_value_changed(
    GtkSpinButton *spin_button,
    gpointer       user_data)
{
  rc_data.det_squelch =
    gtk_spin_button_get_value_as_int( spin_button );
}


  void
on_ratio_spinbutton_value_changed(
    GtkSpinButton *spin_button,
    gpointer       user_data)
{
  rc_data.det_ratio =
    gtk_spin_button_get_value( spin_button );
}


  void
on_error_quit_button_clicked(
    GtkButton *button,
    gpointer   user_data)
{
  Cleanup();
  gtk_main_quit();
}


  void
on_clear_button_clicked(
    GtkButton *button,
    gpointer   user_data)
{
  gtk_text_buffer_set_text( rx_text_buffer, "", -1 );
}


  gboolean
on_waterfall_drawingarea_draw(
    GtkWidget *widget,
    cairo_t   *cr,
    gpointer   user_data)
{
  if( wfall_pixbuf != NULL )
  {
    gdk_cairo_set_source_pixbuf( cr, wfall_pixbuf, 0.0, 0.0 );
    cairo_paint( cr );
  }
  return( TRUE );
}


  gboolean
on_waterfall_drawingarea_button_press_event(
    GtkWidget      *widget,
    GdkEventButton *event,
    gpointer        user_data)
{
  if( event->button == 1 )
  {
    if( isFlagSet(CAT_SETUP) )
      Tune_Tcvr( event->x );
  }
  return( TRUE );
}


  void
on_error_ok_button_clicked(
    GtkButton *button,
    gpointer   user_data)
{
  gtk_widget_destroy( error_dialog );
}


  void
on_error_dialog_destroy(
    GObject  *object,
    gpointer  user_data)
{
  error_dialog = NULL;
}


  gboolean
on_error_dialog_delete_event(
    GtkWidget *widget,
    GdkEvent  *event,
    gpointer   user_data)
{
  return( TRUE );
}


  void
on_auto_checkbutton_toggled(
    GtkToggleButton *togglebutton,
    gpointer         user_data)
{
  if( gtk_toggle_button_get_active(togglebutton) )
    Set_Flag( ADAPT_SPEED );
  else
    Clear_Flag( ADAPT_SPEED );
}


  void
on_waterfall_drawingarea_configure_event(
    GtkWidget    *widget,
    GdkEventConfigure *event,
    gpointer     user_data )
{
  Waterfall_Configure_Event( event->width, event->height );
}


  void
on_receive_togglebutton_toggled(
    GtkToggleButton *togglebutton,
    gpointer         user_data)
{
  if( gtk_toggle_button_get_active(togglebutton) &&
      isFlagSet(RCCONFIG_OK) )
  {
    char mesg[MESG_SIZE];
    int error = 0;
    mesg[0]   = '\0';

    /* Enable receiving Morse code */
    if( !Setup_Sound_Card(mesg, &error) )
    {
      if( error )
      {
        Strlcat( mesg, _("\nError: "), sizeof(mesg) );
        Strlcat( mesg, snd_strerror(error), sizeof(mesg) );
      }
      Cleanup();
      Error_Dialog( mesg );
    }
    else if( isFlagSet(ENABLE_RECEIVE) ) /* Detector initialiazed OK */
    {
      if( isFlagSet(ENABLE_CAT) ) /* Valid receiver specified */
      {
#ifdef HAVE_LIBPERSEUS_SDR
        if( rc_data.receiver_type == PERSEUS )
        {
          if( !Perseus_Initialize() ) return;
        }
        else
#endif
          Open_Tcvr_Serial();
      }

      /* Start receiving and decoding signals */
      g_idle_add( Print_Character, "RX" );
    }
    else
      Error_Dialog( _("Failed to initialize signal detector") );
  } /* if( gtk_toggle_button_get_active(togglebutton) */
  else
  {
    if( isFlagSet(ENABLE_RECEIVE) )
    {
      g_idle_remove_by_data( "RX" );

#ifdef HAVE_LIBPERSEUS_SDR
      if( rc_data.receiver_type == PERSEUS )
        Perseus_Close_Device();
      else
#endif
        if( isFlagSet(ENABLE_CAT) )
          Close_Tcvr_Serial();

      Close_PCM_Handle();
    } /* if( isFlagSet(ENABLE_RECEIVE) ) */
  } /* else of if( gtk_toggle_button_get_active(togglebutton) */
}


  void
on_ratio_radiobutton_toggled(
    GtkToggleButton *togglebutton,
    gpointer         user_data)
{
  if( gtk_toggle_button_get_active(togglebutton) )
  {
    gtk_label_set_text( GTK_LABEL(scope_label), _("Lead/Trail Ratio") );
    Clear_Flag(
        SELECT_LEVEL   |
        DISPLAY_LEVEL  |
        DISPLAY_SIGNAL);
    Set_Flag( DISPLAY_RATIO );
  }
}


  void
on_level_radiobutton_toggled(
    GtkToggleButton *togglebutton,
    gpointer         user_data)
{
  if( gtk_toggle_button_get_active(togglebutton) )
  {
    gtk_label_set_text( GTK_LABEL(scope_label), _("Squelch Level") );
    Set_Flag( SELECT_LEVEL );
    Set_Flag( DISPLAY_LEVEL );
    Clear_Flag( DISPLAY_RATIO | DISPLAY_SIGNAL );
  }
}


  void
on_stop_radiobutton_toggled(
    GtkToggleButton *togglebutton,
    gpointer         user_data)
{
  if( gtk_toggle_button_get_active(togglebutton) )
  {
    gtk_label_set_text( GTK_LABEL(scope_label), _("Hold Display") );
    Clear_Flag(
        DISPLAY_SIGNAL |
        SELECT_LEVEL   |
        DISPLAY_LEVEL  |
        DISPLAY_RATIO );
    Set_Flag( SCOPE_HOLD );
  }
  else Clear_Flag( SCOPE_HOLD );
}


  void
on_signal_radiobutton_toggled(
    GtkToggleButton *togglebutton,
    gpointer         user_data)
{
  if( gtk_toggle_button_get_active(togglebutton) )
  {
    gtk_label_set_text( GTK_LABEL(scope_label), _("Signal Detector") );
    Clear_Flag(
        SELECT_LEVEL  |
        DISPLAY_LEVEL |
        DISPLAY_RATIO );
    Set_Flag( DISPLAY_SIGNAL );
  }
}


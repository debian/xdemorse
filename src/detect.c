/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */


#include "detect.h"
#include "shared.h"

/* Buffers and data needed by tone detector */
static struct
{
  int
    frag_len,
    samples_buff_len, /* Length of DSP samples buffer */
    samples_buff_idx; /* Sample buffer index */

  /* Circular signal samples buffer for Goertzel detector */
  short *samples_buff;

  /* Variables for the Goertzel algorithm */
  double cosw, sinw, coeff;

  /* Circular signal level buffer and index */
  int *sig_level_buff;
  int sig_level_idx;

  unsigned char *state;
  int state_idx;

} detector_data;

static int
  frag_level,   /* Level of audio signal over a fragment */
  level_sum,    /* Sum of above two levels, used as squelch */
  det_ratio;    /* Ratio of trailing edge / leading edge above */

/* Points to plot on scope */
static GdkPoint *points = NULL;

/*------------------------------------------------------------------------*/

/*  Get_Fragment()
 *
 *  Detects the cw beat frequency signal o/p from the radio
 *  receiver and determines the status, (mark or space) of a
 *  'fragment' (a small fraction, ~1/8) of a Morse code element.
 *  Signal detection is done by using a Goertzel algorithm.
 */

  gboolean
Get_Fragment( void )
{
  int
    block_size, /* Block size (N) of the Goertzel algorithm */
    frag_timer, /* Counter for timing duration of fragment  */
    lead_edge,  /* Level of signal's leading edge integrator */
    trail_edge, /* Level of signal's trailing edge integrator */
    ref_ratio,  /* Reference for above to determine Mark | Space */
    state_cnt,  /* Count of the detected signal state above */
    plot = 0,   /* Value to be plotted in xcope */
    idx, ids;

  static unsigned char state; /* Current state of signal */

  /* Variables for the Goertzel algorithm */
  double q0, q1, q2, level;

  /* Index to points to plot on scope */
  static int points_idx = 0;

  /* Initialize on first call */
  if( points == NULL )
  {
    if( !mem_alloc( (void *)&points, (size_t)scope_width * sizeof(GdkPoint)) )
      return( FALSE );
  }

  /* Goertzel block size depends on Morse speed */
  block_size = detector_data.frag_len * rc_data.unit_elem / 2;

  /* Buffer dsp samples of input signal for a fragment */
  for( frag_timer = 0; frag_timer < detector_data.frag_len; frag_timer++ )
  {
#ifdef HAVE_LIBPERSEUS_SDR
    if( rc_data.receiver_type == PERSEUS )
    {
      /* Get new sample from Perseus DSP */
      if( !Demodulate_CW( &detector_data.samples_buff[detector_data.samples_buff_idx]) )
        return( FALSE );
    }
    else
#endif
    {
      /* Get new sample from DSP buffer */
      if( !Signal_Sample(
            &detector_data.samples_buff[detector_data.samples_buff_idx]) )
        return( FALSE );
    }

    /* Advance/Reset circular buffers' index */
    detector_data.samples_buff_idx++;
    if( detector_data.samples_buff_idx >= detector_data.samples_buff_len )
      detector_data.samples_buff_idx = 0;

  } /* for( frag_timer = 0; frag_timer < frag_len ... */

  /** Calculate signal fragment level over a block **/
  /* Backstep buffer index for use of samples */
  detector_data.samples_buff_idx -= block_size;
  if( detector_data.samples_buff_idx < 0 )
    detector_data.samples_buff_idx += detector_data.samples_buff_len;

  /* Calculate fragment level using Goertzel algorithm */
  q1 = q2 = 0.0;
  for( idx = 0; idx < block_size; idx++ )
  {
    q0 = detector_data.coeff * q1 - q2 +
      (double)detector_data.samples_buff[detector_data.samples_buff_idx];
    q2 = q1;
    q1 = q0;

    /* Advance/Reset circular buffers' index */
    detector_data.samples_buff_idx++;
    if( detector_data.samples_buff_idx >= detector_data.samples_buff_len )
      detector_data.samples_buff_idx = 0;

  } /* for( idx = 0; idx < block_size; idx++ ) */

  /* Scalar magnitude of input signal scaled by block size */
  q1 /= (double)block_size;
  q2 /= (double)block_size;
  level = q1*q1 + q2*q2 - q1*q2*detector_data.coeff;
  double sq = sqrt( level );
  frag_level = (int)sq;

  /* Advance/Reset circular buffers' index */
  detector_data.sig_level_idx++;
  if( detector_data.sig_level_idx >= rc_data.max_unit_x2 )
    detector_data.sig_level_idx = 0;

  /* Save signal power level to circular buffer */
  detector_data.sig_level_buff[detector_data.sig_level_idx] = frag_level;

  /* Summate the level of leading edge of signal */
  ids = detector_data.sig_level_idx;
  lead_edge = 0;
  for( idx = 0; idx < rc_data.unit_elem; idx++ )
  {
    lead_edge += detector_data.sig_level_buff[ids];
    ids--;
    if( ids < 0 ) ids += rc_data.max_unit_x2;
  }

  /* Summate the level of trailing edge of signal */
  trail_edge = 0;
  for( idx = 0; idx < rc_data.unit_elem; idx++ )
  {
    trail_edge += detector_data.sig_level_buff[ids];
    ids--;
    if( ids < 0 ) ids += rc_data.max_unit_x2;
  }

  /* Scale lead and trail sums by length of unit element */
  lead_edge /= rc_data.unit_elem;
  trail_edge /= rc_data.unit_elem;

  /* Set signal tone detector status */
  level_sum = lead_edge + trail_edge;
  ref_ratio = (int)( rc_data.det_ratio * RATIO_MULTIPLIER );

  /* Advance/Reset detector state buffer index */
  detector_data.state_idx++;
  if( detector_data.state_idx >= rc_data.max_unit )
    detector_data.state_idx = 0;

  /* Determine state of signal, Mark | Space, if sum is above
   * squelch level. A mark state is denoted by a value of 20,
   * as further below this effectively multiplies the sum of
   * mark states by 20 and simplifies thresholding further below */
  if( level_sum > rc_data.det_squelch * SQUELCH_MULTIPLIER )
  {
    if( ((int)RATIO_MULTIPLIER) * lead_edge > ref_ratio * trail_edge )
      state = 20; /* State is a Mark, e.g. a "Dit" or "Dah" tone */
    else if( ((int)RATIO_MULTIPLIER) * trail_edge > ref_ratio * lead_edge )
      state = 0; /* State is a Space, e.g. no tone */
  } /* if( (lead_edge + trail_edge) > LEVEL_THRESHOLD ) */

  /* Save Mark | Space detector state */
  detector_data.state[detector_data.state_idx] = state;

  /* Summate detector states for the
   * duration of a unit element (dit) */
  ids = detector_data.state_idx;
  state_cnt = 0;
  for( idx = 0; idx < rc_data.unit_elem; idx++ )
  {
    state_cnt += detector_data.state[ids];
    ids--;
    if( ids < 0 ) ids += rc_data.max_unit;
  }

  /* If state count is > 11/20 of unit element, call it a
   * Mark. state is already * 20 at detector further above.
   * If state count is < 9/20 of unit element, call it a
   * Space. This is sort of a thresholding to combat noise */
  if( (int)state_cnt > 11 * rc_data.unit_elem )
  {
    Set_Flag( MARK_TONE );
    Clear_Flag( SPACE_TONE );
  }
  else if( (int)state_cnt < 9 * rc_data.unit_elem )
  {
    Clear_Flag( MARK_TONE );
    Set_Flag( SPACE_TONE );
  }

  /* Lead/Trail ratio for the Scope display */
  det_ratio = 1;
  if( (lead_edge > trail_edge) && trail_edge )
    det_ratio = ((int)RATIO_MULTIPLIER * lead_edge) / trail_edge;
  else if( lead_edge )
    det_ratio = ((int)RATIO_MULTIPLIER * trail_edge) / lead_edge;

  /* Display signal or detector graph (scope) */
  if( isFlagSet(DISPLAY_RATIO) )
  {
    rc_data.det_threshold = (int)( rc_data.det_ratio * RATIO_MULTIPLIER );
    plot = det_ratio;
  }
  else if( isFlagSet(DISPLAY_LEVEL) )
  {
    rc_data.det_threshold = rc_data.det_squelch;
    plot = level_sum / SQUELCH_MULTIPLIER;
  }
  else if( isFlagSet(DISPLAY_SIGNAL) )
    plot = frag_level / SIGNAL_SCALE;

  if( isFlagClear(SCOPE_HOLD) )
  {
    /* Save values to be plotted  */
    points[points_idx].y = scope_height - plot - 1;
    if( points[points_idx].y <= 0 )
      points[points_idx].y = 1;
    if( points[points_idx].y >= scope_height )
      points[points_idx].y = scope_height - 1;
    points[points_idx].x = points_idx;

    /* Recycle buffer idx when full and plot */
    points_idx++;
    if( points_idx >= scope_width )
    {
      /* Draw scope */
      Set_Flag( SCOPE_READY );
      gtk_widget_queue_draw( scope );
      points_idx = 0;
    }
  }

  return( TRUE );
} /* End of Get_Fragment() */

/*------------------------------------------------------------------------*/

/* Display_Scope()
 *
 * Displays graphs in the scope display on "draw" signal
 */
  void
Display_Scope( cairo_t *cr )
{
  if( isFlagSet(SCOPE_READY) )
  {
    /* Display signal or detector graph (scope) */
    if( isFlagSet(DISPLAY_RATIO) ||
        isFlagSet(DISPLAY_LEVEL) )
      Display_Detector( cr, points );
    else if( isFlagSet(DISPLAY_SIGNAL) )
      Display_Signal( cr, points );
    Clear_Flag( SCOPE_READY );
  }
  else /* Clear scope */
  {
    /* Draw scope background */
    cairo_set_source_rgb( cr, RGB_BACKGND );
    cairo_rectangle(
        cr, 0.0, 0.0,
        (double)scope_width  + 2.0,
        (double)scope_height + 2.0 );
    cairo_fill( cr );
  }

} /* Display_Scope() */

/*------------------------------------------------------------------------*/

/* Initialize_Detector()
 *
 * Initializes variables and allocates buffers
 * for the Goertzel tone detector
 */
  gboolean
Initialize_Detector( void )
{
  size_t alloc;
  int idx;

  /* Goertzel detector coefficients */
  double w = 2.0 * M_PI *
    (double)rc_data.tone_freq / (double)rc_data.dsp_rate;
  detector_data.cosw  = cos(w);
  detector_data.sinw  = sin(w);
  detector_data.coeff = 2.0 * detector_data.cosw;

  /* Clear buffer indices */
  detector_data.samples_buff_idx = 0;
  detector_data.sig_level_idx = 0;
  detector_data.state_idx = 0;

  /* Lenght of a signal 'fragment', a small part of one Morse element */
  detector_data.frag_len = (rc_data.dsp_rate * CYCLES_PER_FRAG) / rc_data.tone_freq;

  /* Length of DSP samples buffer for Goertzel detector */
  detector_data.samples_buff_len =
    (CYCLES_PER_FRAG * rc_data.max_unit * rc_data.dsp_rate) / rc_data.tone_freq;

  /* Allocate samples buffer */
  detector_data.samples_buff = NULL;
  alloc = (size_t)detector_data.samples_buff_len * sizeof(short);
  if( !mem_alloc((void **)&detector_data.samples_buff, alloc) )
    return( FALSE );

  /* Clear buffer */
  for( idx = 0; idx < detector_data.samples_buff_len; idx++ )
    detector_data.samples_buff[idx] = 0;

  /* Allocate signal level (Goerstzel detector o/p) buffer */
  detector_data.sig_level_buff = NULL;
  alloc = (size_t)rc_data.max_unit_x2 * sizeof(int);
  if( !mem_alloc( (void **)&detector_data.sig_level_buff, alloc) )
    return( FALSE );

  /* Clear buffer */
  for( idx = 0; idx < rc_data.max_unit_x2; idx++ )
    detector_data.sig_level_buff[idx] = 0;

  /* Allocate signal level (Goerstzel detector o/p) buffer */
  detector_data.state = NULL;
  alloc = (size_t)rc_data.max_unit;
  if( !mem_alloc( (void **)&detector_data.state, alloc) )
    return( FALSE );

  /* Clear buffer */
  for( idx = 0; idx < rc_data.max_unit; idx++ )
    detector_data.state[idx] = 0;

  return( TRUE );
} /* Initialize_Detector() */

/*------------------------------------------------------------------------*/


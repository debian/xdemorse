/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#ifndef INTERFACE_H
#define INTERFACE_H     1

#include "common.h"


/* Object ID's for GtkBuilder */
#define ERROR_DIALOG_IDS \
"error_dialog", \
"error_quit_button", \
"error_ok_button", \
"error_message", \
NULL

#define MAIN_WINDOW_IDS \
"freq_adjustment", \
"level_adjustment", \
"ratio_adjustment", \
"wpm_adjustment", \
"main_window", \
"rx_scrolledwindow", \
"rx_textview", \
"scope_drawingarea", \
"scope_label", \
"receive_togglebutton", \
"frequency_spinbutton", \
"bandwidth_combobox", \
"clear_button", \
"receiver_combobox", \
"auto_checkbutton", \
"wpm_spinbutton", \
"signal_radiobutton", \
"level_radiobutton", \
"ratio_radiobutton", \
"stop_radiobutton", \
"squelch_spinbutton", \
"ratio_spinbutton", \
"waterfall_drawingarea", \
NULL

#endif


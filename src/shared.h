/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#ifndef SHARED_H
#define SHARED_H    1

#include "common.h"

/*------------------------------------------------------------------------*/

/* Global widgets */
extern GtkWidget
  *rx_scrolledwindow,
  *scope,
  *waterfall,
  *scope_label;

/* Receiver type combobox */
extern GtkComboBoxText *receiver_combobox;

/* Speed and squelch spin button */
extern GtkSpinButton
  *frequency,
  *speed,
  *squelch,
  *ratio;

/* Main window builder */
extern GtkBuilder *main_builder;

/* Runtime config data */
extern rc_data_t rc_data;

/* DSP samples buffer */
extern samples_buffer_t samples_buffer;

/* Waterfall window pixbuf */
extern GdkPixbuf *wfall_pixbuf;
extern guchar *wfall_pixels;
extern gint
  wfall_rowstride,
  wfall_n_channels,
  wfall_width,
  wfall_height;

/* Text buffer for text viewer */
extern GtkTextBuffer *rx_text_buffer;

extern gint
  scope_width,
  scope_height;

/* Average bin values */
extern int *bin_ave;

/* ifft in/out buffers */
extern int16_t
  *ifft_data,
  ifft_data_length;

extern sem_t pback_semaphore;

/* Sound Playback Buffer has to be a ring
 * buffer to allow for the differences in the
 * SDR ADC and Sound card ADC sampling rates */
#define PLAYBACK_BUF_LEN    8192
extern short **playback_buf;
extern int   sound_buf_num;
extern int   demod_buf_num;

/*------------------------------------------------------------------------*/

#endif

/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include "utils.h"
#include "shared.h"

/*------------------------------------------------------------------*/

/* Functions for testing and setting/clearing flow control flags
 *
 *  See xdemorse.h for definition of flow control flags
 */

/* An int variable holding the single-bit flags */
static int Flags = 0;

  int
isFlagSet( int flag )
{
  return( (Flags & flag) == flag );
}

  int
isFlagClear( int flag )
{
  return( (~Flags & flag) == flag );
}

  void
Set_Flag( int flag )
{
  Flags |= flag;
}

  void
Clear_Flag( int flag )
{
  Flags &= ~flag;
}

  void
Toggle_Flag( int flag )
{
  Flags ^= flag;
}

/*------------------------------------------------------------------------*/

/*  Cleanup()
 *
 *  Cleans up before quitting
 */
  void
Cleanup( void )
{
#ifdef HAVE_LIBPERSEUS_SDR
  if( rc_data.receiver_type == PERSEUS )
    Perseus_Close_Device();
  else
#endif
    Close_Tcvr_Serial();

  /* Release sound card */
  Close_PCM_Handle();
  Close_Mixer_Handle();
} /* Cleanup( void ) */

/*------------------------------------------------------------------------*/


/***  Memory allocation/freeing utils ***/
gboolean mem_alloc( void **ptr, size_t req )
{
  free_ptr( ptr );
  *ptr = malloc( req );
  if( *ptr == NULL )
  {
    perror( "xdemorse: alloc():" );
    Error_Dialog(
        _("A memory allocation failed\n"\
          "Please quit xdemorse and correct")  );
    return( FALSE );
  }
  memset( *ptr, 0, req );

  return( TRUE );
} /* End of void mem_alloc() */

/*------------------------------------------------------------------------*/

gboolean mem_realloc( void **ptr, size_t req )
{
  *ptr = realloc( *ptr, req );
  if( *ptr == NULL )
  {
    perror( "xdemorse: realloc():" );
    Error_Dialog(
        _("A memory re-allocation failed\n"\
          "Please quit xdemorse and correct") );
    return( FALSE );
  }

  return( TRUE );
} /* End of void mem_realloc() */

/*------------------------------------------------------------------------*/

void free_ptr( void **ptr )
{
  if( *ptr != NULL )
    free( *ptr );
  *ptr = NULL;

} /* End of void free_ptr() */

/*------------------------------------------------------------------------*/

/* Strlcpy()
 *
 * Copies n-1 chars from src string into dest string. Unlike other
 * such library fuctions, this makes sure that the dest string is
 * null terminated by copying only n-1 chars to leave room for the
 * terminating char. n would normally be the sizeof(dest) string but
 * copying will not go beyond the terminating null of src string
 */
  void
Strlcpy( char *dest, const char *src, size_t n )
{
  int idx = 0;

  /* Leave room for terminating null in dest */
  n--;

  /* Copy till terminating null of src or to n-1 */
  while( (src[idx] != '\0') && (n > 0) )
  {
    dest[idx] = src[idx];
    idx++;
    n--;
  }

  /* Terminate dest string */
  dest[idx] = '\0';

} /* Strlcpy() */

/*------------------------------------------------------------------*/

/* Strlcat()
 *
 * Concatenates at most n-1 chars from src string into dest string.
 * Unlike other such library fuctions, this makes sure that the dest
 * string is null terminated by copying only n-1 chars to leave room
 * for the terminating char. n would normally be the sizeof(dest)
 * string but copying will not go beyond the terminating null of src
 */
  void
Strlcat( char *dest, const char *src, size_t n )
{
  char ch = dest[0];
  int idd = 0; /* dest index */
  int ids = 0; /* src  index */

  /* Find terminating null of dest */
  while( (n > 0) && (ch != '\0') )
  {
    idd++;
    ch = dest[idd];
    n--; /* Count remaining char's in dest */
  }

  /* Copy n-1 chars to leave room for terminating null */
  n--;
  ch = src[ids];
  while( (n > 0) && (ch != '\0') )
  {
    dest[idd] = src[ids];
    ids++;
    ch = src[ids];
    idd++;
    n--;
  }

  /* Terminate dest string */
  dest[idd] = '\0';

} /* Strlcat() */

/*------------------------------------------------------------------*/

/*  Load_Line()
 *
 *  Loads a line from a file, aborts on failure. Lines beginning
 *  with a '#' are ignored as comments. At the end of file EOF is
 *  returned. Lines assumed maximum 80 characters long.
 */

  static int
Load_Line( char *buff, FILE *pfile, char *mesg )
{
  int
    num_chr, /* Number of characters read, excluding lf/cr */
    chr;     /* Character read by getc() */
  char error_mesg[MESG_SIZE];

  /* Prepare error message */
  snprintf( error_mesg, MESG_SIZE,
      _("Error reading %s\n"\
        "Premature EOF (End Of File)"), mesg );

  /* Clear buffer at start */
  buff[0] = '\0';
  num_chr = 0;

  /* Get next character, return error if chr = EOF */
  if( (chr = fgetc(pfile)) == EOF )
  {
    fprintf( stderr, "xdemorse: %s\n", error_mesg );
    fclose( pfile );
    Error_Dialog( error_mesg );
    return( EOF );
  }

  /* Ignore commented lines, white spaces and eol/cr */
  while(
      (chr == '#') ||
      (chr == ' ') ||
      (chr == HT ) ||
      (chr == CR ) ||
      (chr == LF ) )
  {
    /* Go to the end of line (look for LF or CR) */
    while( (chr != CR) && (chr != LF) )
      /* Get next character, return error if chr = EOF */
      if( (chr = fgetc(pfile)) == EOF )
      {
        fprintf( stderr, "xdemorse: %s\n", error_mesg );
        fclose( pfile );
        Error_Dialog( error_mesg );
        return( EOF );
      }

    /* Dump any CR/LF remaining */
    while( (chr == CR) || (chr == LF) )
      /* Get next character, return error if chr = EOF */
      if( (chr = fgetc(pfile)) == EOF )
      {
        fprintf( stderr, "xdemorse: %s\n", error_mesg );
        fclose( pfile );
        Error_Dialog( error_mesg );
        return( EOF );
      }

  } /* End of while( (chr == '#') || ... */

  /* Continue reading characters from file till
   * number of characters = 80 or EOF or CR/LF */
  while( num_chr < 80 )
  {
    /* If LF/CR reached before filling buffer, return line */
    if( (chr == LF) || (chr == CR) ) break;

    /* Enter new character to line buffer */
    buff[num_chr++] = (char)chr;

    /* Get next character */
    if( (chr = fgetc(pfile)) == EOF )
    {
      /* Terminate buffer as a string if chr = EOF */
      buff[num_chr] = '\0';
      return( SUCCESS );
    }

    /* Abort if end of line not reached at 80 char. */
    if( (num_chr == 80) && (chr != LF) && (chr != CR) )
    {
      /* Terminate buffer as a string */
      buff[num_chr] = '\0';
      snprintf( error_mesg, MESG_SIZE,
          _("Error reading %s\n"\
            "Line longer than 80 characters"), mesg );
      fprintf( stderr, "xdemorse: %s\n%s\n", error_mesg, buff );
      fclose( pfile );
      Error_Dialog( error_mesg );
      return( ERROR );
    }

  } /* End of while( num_chr < max_chr ) */

  /* Terminate buffer as a string */
  buff[num_chr] = '\0';

  return( SUCCESS );
} /* End of Load_Line() */

/*------------------------------------------------------------------*/

/*  Load_Config()
 *
 *  Loads the xdemorserc configuration file
 */

  gboolean
Load_Config( gpointer data )
{
  char
    rc_fpath[64], /* File path to xdemorserc */
    line[81];     /* Buffer for Load_Line  */

  /* Config file pointer */
  FILE *xdemorserc;

  /* ALSA channel names and values */
  char *chan_name[] =
  {
    "FRONT_LEFT",
    "FRONT_RIGHT",
    "REAR_LEFT",
    "REAR_RIGHT",
    "SIDE_LEFT",
    "SIDE_RIGHT",
    "MONO"
  };

  int chan_number[] =
  {
    SND_MIXER_SCHN_FRONT_LEFT,
    SND_MIXER_SCHN_FRONT_RIGHT,
    SND_MIXER_SCHN_REAR_LEFT,
    SND_MIXER_SCHN_REAR_RIGHT,
    SND_MIXER_SCHN_SIDE_LEFT,
    SND_MIXER_SCHN_SIDE_RIGHT,
    SND_MIXER_SCHN_MONO
  };

  int idx, num_chan = 7;

  /* Setup file path to xdemorserc */
  snprintf( rc_fpath, sizeof(rc_fpath),
      "%s/.xdemorse/xdemorserc", getenv("HOME") );

  /* Open xdemorserc file */
  xdemorserc = fopen( rc_fpath, "r" );
  if( xdemorserc == NULL )
  {
    perror( rc_fpath );
    Error_Dialog(
        _("Failed to open .xdemorse/xdemorserc file.\n"\
          "Quit xdemorse and correct.\n"\
          "(You may need to copy the file:\n"\
          "/usr/local/share/examples/xdemorse/xdemorserc.example\n"\
          "to your <home-directory>/.xdemorse/xdemorserc and edit.)") );
    return( FALSE );
  }

  /*** Read runtime configuration data ***/
  /* Read card name, abort if EOF */
  if( Load_Line(line, xdemorserc, _("Card Name")) != SUCCESS )
    return( FALSE );
  Strlcpy( rc_data.snd_card, line, sizeof(rc_data.snd_card) );

  /* Read DSP rate Samples/sec, abort if EOF */
  if( Load_Line(line, xdemorserc, _("DSP Rate") ) != SUCCESS )
    return( FALSE );
  rc_data.dsp_rate = atoi( line );

  /* Read ALSA "channel", abort if EOF */
  if( Load_Line(line, xdemorserc, _("ALSA Channel") ) != SUCCESS )
    return( FALSE );
  for( idx = 0; idx < num_chan; idx++ )
    if( strcmp(chan_name[idx], line) == 0 )
      break;
  if( idx == num_chan )
  {
    fclose( xdemorserc );
    Error_Dialog(
        _("Invalid ALSA channel name\n"\
          "Quit and correct xdemorserc") );
    return( FALSE );
  }
  rc_data.channel = chan_number[idx];

  /* Set right or left channel buffer index */
  if( strstr(line, "LEFT") || strstr(line, "MONO") )
    rc_data.use_chnl = 0;
  else
    rc_data.use_chnl = 1;

  /* Read capture source, abort if EOF */
  if( Load_Line(line, xdemorserc, _("Capture Source")) != SUCCESS )
    return( FALSE );
  Strlcpy( rc_data.cap_src, line, sizeof(rc_data.cap_src) );

  /* Read Capture volume control, abort if EOF */
  if( Load_Line(line, xdemorserc, _("Capture Volume Control")) != SUCCESS )
    return( FALSE );
  Strlcpy( rc_data.cap_vol, line, sizeof(rc_data.cap_vol) );

  /* Read Capture volume, abort if EOF */
  if( Load_Line(line, xdemorserc, _("Capture Volume")) != SUCCESS )
    return( FALSE );
  rc_data.cap_level = atoi( line );

  /* Read max WPM, abort if EOF */
  if( Load_Line(line, xdemorserc, _("Maximum WPM")) != SUCCESS )
    return( FALSE );
  rc_data.min_unit = atoi( line );

  /* Read min WPM, abort if EOF */
  if( Load_Line(line, xdemorserc, _("Minimum WPM")) != SUCCESS )
    return( FALSE );
  rc_data.max_unit = atoi( line );

  /* Check range of Morse speeds. At this point max_unit
   * holds minimum wpm and min_unit holds maximum wpm */
  if( (rc_data.max_unit < MIN_SPEED) ||
      (rc_data.min_unit > MAX_SPEED) )
  {
    fclose( xdemorserc );
    Error_Dialog(
        _("Morse code speed (WPM)\n"\
          "range is out of limits\n"\
          "Quit and correct xdemorserc") );
    return( FALSE );
  }

  /* Set spinbutton range */
  gtk_spin_button_set_range( speed,
      (gdouble)rc_data.max_unit, (gdouble)rc_data.min_unit );

  /* Read and check initial WPM, abort if EOF */
  if( Load_Line(line, xdemorserc, _("Initial WPM")) != SUCCESS )
    return( FALSE );
  int wpm = atoi( line );

  /* Check initial Morse speed. At this point max_unit
   * holds minimum wpm and min_unit holds maximum wpm */
  if( (wpm > rc_data.min_unit) ||
      (wpm < rc_data.max_unit) )
  {
    fclose( xdemorserc );
    Error_Dialog(
        _("Initial Morse code speed (WPM)\n"\
          "is out of specified range\n"\
          "Quit and correct xdemorserc") );
    return( FALSE );
  }

  /* Read CAT serial port device, abort if EOF */
  if( Load_Line(line, xdemorserc, _("CAT Serial Port")) != SUCCESS )
    return( FALSE );
  Strlcpy( rc_data.cat_serial, line, sizeof(rc_data.cat_serial) );

  /* Read CAT enable flag, abort if EOF */
  if( Load_Line(line, xdemorserc, _("Transceiver type for CAT")) != SUCCESS )
    return( FALSE );
  if( strcmp(line, "FT847") == 0 )
    rc_data.receiver_type = FT847;
  else if( strcmp(line, "FT857") == 0 )
    rc_data.receiver_type = FT857;
  else if( strcmp(line, "K2") == 0 )
    rc_data.receiver_type = K2;
  else if( strcmp(line, "K3") == 0 )
    rc_data.receiver_type = K3;
#ifdef HAVE_LIBPERSEUS_SDR
  else if( strcmp(line, "PERSEUS") == 0 )
    rc_data.receiver_type = PERSEUS;
#endif
  else if( strcmp(line, "NONE") == 0 )
    rc_data.receiver_type = NONE;
  else
  {
    rc_data.receiver_type = NONE;
    fclose( xdemorserc );
    Error_Dialog(
        _("Error reading Transceiver type\n"\
          "Quit and correct xdemorserc") );
    return( FALSE );
  }
  gtk_combo_box_set_active( GTK_COMBO_BOX(receiver_combobox), rc_data.receiver_type );
  if( rc_data.receiver_type == PERSEUS )
    rc_data.open_type = SND_OPEN_PLAYBACK;
  else
    rc_data.open_type = SND_OPEN_CAPTURE;

  /* Read frequency correction factor */
  if( Load_Line(line, xdemorserc, _("Frequency Correction Factor")) != SUCCESS )
    return( FALSE );
  rc_data.freq_correction = atof( line );
  if( fabs(rc_data.freq_correction) > 100.0 )
  {
    fclose( xdemorserc );
    Error_Dialog(
        _("Error reading Frequency Correction Factor\n"\
          "Frequency Correction Factor seems excessive\n"\
          "Quit and correct xdemorserc") );
    return( FALSE );
  }
  rc_data.freq_correction /= 1.0E6;

  /* Read input tone (Rx BFO) frequency */
  if( Load_Line(line, xdemorserc, _("Input Tone")) != SUCCESS )
    return( FALSE );
  rc_data.tone_freq = atoi( line );
  if( rc_data.tone_freq < 200 )
  {
    fclose( xdemorserc );
    Error_Dialog(
        _("Error reading BFO Tone Freq\n"\
          "Quit and correct xdemorserc") );
    return( FALSE );
  }

  /* Get sizes of displays */
  GtkAllocation alloc;
  gtk_widget_get_allocation( waterfall, &alloc );
  Waterfall_Configure_Event( alloc.width, alloc.height );

  gtk_widget_get_allocation( scope, &alloc );
  scope_width  = alloc.width  - 2;
  scope_height = alloc.height - 2;

  /* Set spinbutton value */
  gtk_spin_button_set_value( speed, (gdouble)wpm );
  rc_data.unit_elem = (60 * rc_data.tone_freq) / (50 * wpm * CYCLES_PER_FRAG);
  rc_data.center_line = wfall_width / 2 + 1;

  /* Enable Transceiver CAT */
  if( rc_data.receiver_type == NONE )
    Clear_Flag( ENABLE_CAT );
  else
    Set_Flag( ENABLE_CAT );

  /* Allocate memory to recv samples buffer */
  samples_buffer.buffer_size = SND_READI_FRAMES * SND_NUM_CHANNELS;
  samples_buffer.buffer_idx  = samples_buffer.buffer_size;
  samples_buffer.buffer = NULL;
  if( !mem_alloc( (void **)&(samples_buffer.buffer),
        (size_t)samples_buffer.buffer_size * sizeof(short)) )
  {
    fclose( xdemorserc );
    return( FALSE );
  }
  memset( samples_buffer.buffer, 0,
      (size_t)samples_buffer.buffer_size * sizeof(short) );

  Set_Flag( RCCONFIG_OK );
  if( Initialize_Detector() )
    Set_Flag( ENABLE_RECEIVE );

  fclose( xdemorserc );
  return( FALSE );
} /* End of Load_Config() */

/*------------------------------------------------------------------*/

/* Waterfall_Configure_Event()
 *
 * Handles the "configure" event on the waterfall
 */
  void
Waterfall_Configure_Event( int width, int height )
{
  int idx;

  /* Destroy existing pixbuff */
  if( wfall_pixbuf != NULL )
  {
    g_object_unref( G_OBJECT(wfall_pixbuf) );
    wfall_pixbuf = NULL;
  }

  /* Create waterfall pixbuf */
  wfall_pixbuf = gdk_pixbuf_new( GDK_COLORSPACE_RGB, FALSE, 8, width, height );
  if( wfall_pixbuf == NULL )
  {
    Error_Dialog( _("Failed to create pixbuf for waterfall") );
    return;
  }

  wfall_pixels = gdk_pixbuf_get_pixels( wfall_pixbuf );
  wfall_width  = gdk_pixbuf_get_width( wfall_pixbuf );
  wfall_height = gdk_pixbuf_get_height( wfall_pixbuf );
  wfall_rowstride  = gdk_pixbuf_get_rowstride( wfall_pixbuf );
  wfall_n_channels = gdk_pixbuf_get_n_channels( wfall_pixbuf );
  gdk_pixbuf_fill( wfall_pixbuf, 0 );

  /* Allocate average bin value buffer */
  if( !mem_realloc((void **)&bin_ave, (size_t)wfall_width * sizeof(int)) )
    return;
  for( idx = 0; idx < wfall_width; idx++ )
    bin_ave[idx] = 0;

  /* Initialize ifft. The waterfall width has to be odd
   * to provide a center line and the FFT width has to
   * be a power of 2 so we compensate by adding 1 */
  if( !Initialize_IFFT((int16_t)wfall_width + 1) )
    return;

  /* Puts the tone freq in middle of waterfall */
  idx = (2 * ifft_data_length) / wfall_width;

  /* The tone freq must be rounded so that the ifft_stride
   * is an integer otherwise the waterfall is not accurate */
  if( idx )
  {
    rc_data.ifft_stride = rc_data.dsp_rate / rc_data.tone_freq / idx;
    rc_data.tone_freq   = rc_data.dsp_rate / rc_data.ifft_stride / idx;
  }

  /* Calculate parameters that depend on above */
  rc_data.max_unit    = (60 * rc_data.tone_freq) / (50 * rc_data.max_unit * CYCLES_PER_FRAG);
  rc_data.max_unit_x2 = rc_data.max_unit * 2;
  rc_data.min_unit    = (60 * rc_data.tone_freq) / (50 * rc_data.min_unit * CYCLES_PER_FRAG);

} /* Waterfall_Configure_Event() */

/*------------------------------------------------------------------------*/


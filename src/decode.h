/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#ifndef DECODE_H
#define DECODE_H    1

#include "common.h"
#include "detect.h"

/****** CUSTOMIZE THESE DEFINITIONS FOR YOUR NEEDS ******/

/*** Definitions for Morse code conversion to ascii ***/

/* Number of chars in ASCII table, NOT INCLUDING "*" */
#define NUMBER_OF_CHAR  64
#define MAX_CHAR_LEN    5

/* ASCII equivalents to Morse hex code. Last */
/* one (*) used for unrecognized characters. */
#define MORSE_ASCII_CHAR \
  "A","B","C","D","E","F","G","H","I","J","K","L",  \
  "M","N","O","P","Q","R","S","T","U","V","W","X",  \
  "Y","Z","1","2","3","4","5","6","7","8","9","0",  \
  ".",",","?","\"","!","/","(",")","&",":",";","=", \
  "+","-","_","\"","$","@","<OK>","<GO>","<BK>","<SK>", \
  "\u00c4","\u00d6","\u00dc","\u00df","<!!>"," ","*"

/* Hex equivalents to Morse code chars above except (*).  */
/* Formed by starting with a 1 and following with a 0 for */
/* dash and 1 for dit e.g: B = dahdididit = 10111 = Hex 0x17 */
#define MORSE_HEX_CODE \
  0x06,0x17,0x15,0x0b,0x03,0x1d,0x09,0x1f,0x07,0x18,0x0a,0x1b, \
  0x04,0x05,0x08,0x19,0x12,0x0d,0x0f,0x02,0x0e,0x1e,0x0c,0x16, \
  0x14,0x13,0x30,0x38,0x3c,0x3e,0x3f,0x2f,0x27,0x23,0x21,0x20, \
  0x6a,0x4c,0x73,0x61,0x54,0x2d,0x29,0x52,0x37,0x47,0x55,0x2e, \
  0x35,0x5e,0x72,0x6d,0xf6,0x65,0x3d,0x2a,0xba,0x7a,0x1a,0x11, \
  0x1c,0xf3,0xff,0x01

/***************** END OF CUSTOMIZABLE SECTION ***********************/

#define MAX_SPEED       60  /* Maximum allowed Morse unit (dot) length */
#define MIN_SPEED       6   /* Minimum allowed Morse unit (dot) length */

/* Definitions of contexts (stages) in Morse decoding process */
#define NO_CONTEXT      0        /* Context is not defined */
#define MARK_SIGNAL     0x000001 /* Count fragments of a mark element   */
#define ELEM_SPACE      0x000002 /* Count frag. of inter-element space  */
#define CHAR_SPACE      0x000004 /* Count fragments of inter-char space */
#define WAIT_WORD_SPACE 0x000008 /* Wait for an inter-word space        */
#define WORD_SPACE      0x000010 /* Count fragments of inter-word space */
#define WAIT_FOR_MARK   0x000020 /* Count fragments of no-signal space  */
#define WAIT_FOR_SPACE  0x000040 /* Wait for a space after a long dash  */
#define LONG_SPACE      0x000100 /* Long period of space (no tone) */

#define ENTER_DOT       0x01    /* Enter a dash element into Morse decode */

#endif

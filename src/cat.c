/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details:
 *
 *  http://www.gnu.org/copyleft/gpl.txt
 */

#include "cat.h"
#include "shared.h"

static char
  /* Command codes for Yaesu FT847/FT857 CAT */
  FT847_CAT_ON[]    = { 0, 0, 0, 0, (char)0x00 }, /*  Enable CAT */
  FT847_CAT_OFF[]   = { 0, 0, 0, 0, (char)0x80 }, /* Disable CAT */
  FT847_MAIN_VFO[]  = { 0, 0, 0, 0, (char)0x03 }, /* Read main VFO freq */
  FT847_SET_VFO[]   = { 0, 0, 0, 0, (char)0x01 }, /* Set main VFO freq  */
  FT847_MODE_CWR[]  = { (char)0x03, 0, 0, 0, (char)0x07 }, /* Set mode to CWR */
  FT847_MODE_CWNR[] = { (char)0x83, 0, 0, 0, (char)0x07 }, /* Set mode to CWNR */

  /* Command codes for Elecraft K3 */
  K3_K31_S[] = "K31;",              /* Set K3 CAT mode */
  K3_K31_G[] = "K3;",               /* Request K3 CAT mode */
  K3_VFOA_S[] = "FAnnnnnnnnnnn;",   /* Set VFO A frequency */
  K3_VFOA_G[] = "FA;",              /* Get VFO A frequency */
  K3_MODE_CWR[] = "MD7;";           /* Set mode to CWR */

/* Serial port File descriptor */
static int serial_fd = 0;

/* Original serial port options */
static struct termios tcvr_serial_old_options;

/*------------------------------------------------------------------------*/

/* Cancel_CAT()
 *
 * Cancels Transceiver CAT (on error)
 */

  static void
Cancel_CAT( void )
{
  close( serial_fd );
  serial_fd = 0;
  //rc_data.receiver_type = NONE;
  Clear_Flag( CAT_SETUP );
  Clear_Flag( TCVR_SERIAL_TEST );
  Error_Dialog( "Failed to Establish Transceiver CAT"  );
}

/*-------------------------------------------------------------------------*/

/*  Read_Tcvr_Serial()
 *
 *  Reads Data from the Transceiver
 */

  static gboolean
Read_Tcvr_Serial( char *data, size_t len )
{
  int nbytes = 0, cnt = 0;

  /* Check for data to be available */
  if( isFlagClear(TCVR_SERIAL_TEST) && isFlagClear(CAT_SETUP) )
    return( FALSE );
  while( ++cnt < MAX_SERIAL_RETRIES )
  {
    ioctl( serial_fd, FIONREAD, &nbytes);
    if( nbytes == (int)len ) break;
    fprintf( stderr,
        "xdemorse: read(): incorrect number of bytes available:"
        " %d/%d\n", nbytes, (int)len );
    usleep( 100000 );
  }

  /* Error condition */
  if( cnt >= MAX_SERIAL_RETRIES )
  {
    tcflush( serial_fd, TCIOFLUSH );
    fprintf( stderr,
        "xdemorse: read(): incorrect number of bytes available:"
        " %d/%d after %d tries\n", nbytes, (int)len, cnt );
    data[0] = '\0';
    Cancel_CAT();
    return( FALSE );
  }

  /* Clear data buffer and read from serial */
  size_t ret = (size_t)read( serial_fd, data, len );

  /* Error condition */
  if( ret != len )
  {
    tcflush( serial_fd, TCIOFLUSH );
    fprintf( stderr,
        "xdemorse: read() return value wrong: %d/%d\n",
        (int)ret, (int)len );
    data[0] = '\0';
    Cancel_CAT();
    return( FALSE );
  }

  return( TRUE );
} /* End of Read_Tcvr_Serial() */

/*-------------------------------------------------------------------------*/

/*  Write_Tcvr_Serial()
 *
 *  Writes a command to the Tranceiver
 */

  static gboolean
Write_Tcvr_Serial( char *data, size_t len )
{
  int cnt = 0;
  size_t ret = 0;

  /* Flush serial and write command, retrying if needed */
  if( isFlagClear(TCVR_SERIAL_TEST) &&
      isFlagClear(CAT_SETUP) )
    return( FALSE );
  tcflush( serial_fd, TCIOFLUSH );
  while( cnt++ < MAX_SERIAL_RETRIES )
  {
    ret = (size_t)write( serial_fd, data, len );
    if( ret == len ) break;
    fprintf( stderr,
        "xdemorse: write(): incorrect number of bytes written:"
        " %d/%d\n", (int)ret, (int)len );
    usleep( 100000 );
  }

  /* Error condition */
   if( cnt >= MAX_SERIAL_RETRIES )
  {
    tcflush( serial_fd, TCIOFLUSH );
    fprintf( stderr,
        "xdemorse: write(): incorrect number of bytes written:"
        " %d/%d after %d tries\n", (int)ret, (int)len, cnt );
    Cancel_CAT();
    return( FALSE );
  }

  /* Give time to CAT to do its thing */
  usleep( 50000 );
  return( TRUE );

} /* End of Write_Tcvr_Serial() */

/*-------------------------------------------------------------------------*/

/*  Read_Rx_Freq()
 *
 *  Reads the Rx freq of the transceiver.
 */

  static gboolean
Read_Rx_Freq( int *freq )
{
  /* A char string for sending and receiving  */
  /* data strings to and from the transceiver */
  char frq[5];

  /* Abort if CAT disabled */
  if( isFlagClear(CAT_SETUP) ) return( TRUE );

  switch( rc_data.receiver_type )
  {
    case FT847: case FT857:
    /* Read Rx frequency */
    memset( frq, 0, sizeof(frq) );
    if( !Write_Tcvr_Serial(FT847_MAIN_VFO, sizeof(FT847_MAIN_VFO)) ||
        !Read_Tcvr_Serial(frq, sizeof(frq)) )
      return( FALSE );

    /* Decode frequency data to 10Hz */
    *freq = 0;
    *freq += (frq[0] & 0xF0) >> 4;
    *freq *= 10;
    *freq += frq[0] & 0x0F;
    *freq *= 10;
    *freq += (frq[1] & 0xF0) >> 4;
    *freq *= 10;
    *freq += frq[1] & 0x0F;
    *freq *= 10;
    *freq += (frq[2] & 0xF0) >> 4;
    *freq *= 10;
    *freq += frq[2] & 0x0F;
    *freq *= 10;
    *freq += (frq[3] & 0xF0) >> 4;
    *freq *= 10;
    *freq += frq[3] & 0x0F;
    *freq *= 10;
    break;

    case K2: case K3:
    /* Read Rx frequency */
    if( !Write_Tcvr_Serial(K3_VFOA_G, sizeof(K3_VFOA_G)-1) ||
        !Read_Tcvr_Serial(K3_VFOA_S, sizeof(K3_VFOA_S)-1) )
      return( FALSE );
    *freq = atoi( K3_VFOA_S + 2 );
    break;

  } /* switch( rc_data.receiver_type ) */

  return( TRUE );
} /* End of Read_Rx_Freq() */

/*------------------------------------------------------------------------*/

/*  Write_Rx_Freq()
 *
 *  Writes the Rx freq to the transceiver.
 */

  gboolean
Write_Rx_Freq( int freq )
{
  /* Buffer used for converting freq. to string */
  char freq_buff[11];
  int idx; /* Index for loops etc */


  /* Abort if CAT disabled */
  if( isFlagClear(CAT_SETUP) ) return( TRUE );

  switch( rc_data.receiver_type )
  {
    case FT847: case FT857:
      /* Set Rx frequency */
      snprintf( freq_buff, sizeof(freq_buff), "%08d", freq/10 );
      for( idx = 0; idx < 4; idx++ )
      {
        FT847_SET_VFO[idx]  = (char)((freq_buff[2*idx]   - '0') << 4);
        FT847_SET_VFO[idx] |= (char)( freq_buff[2*idx+1] - '0');
      }
      if( !Write_Tcvr_Serial(FT847_SET_VFO, sizeof(FT847_SET_VFO)) )
        return( FALSE );
      break;

    case K2: case K3:
      /* Set Rx frequency */
      snprintf( K3_VFOA_S+2, sizeof(K3_VFOA_S)-2, "%011d;", freq );
      if( !Write_Tcvr_Serial(K3_VFOA_S, sizeof(K3_VFOA_S)-1) )
          return( FALSE );
      break;

#ifdef HAVE_LIBPERSEUS_SDR
     /* Set Perseus frequency */
    case PERSEUS:
      Perseus_Set_Center_Frequency( freq );
      break;
#endif

  } /* switch( rc_data.receiver_type ) */

  return( TRUE );
} /* End of Write_Rx_Freq() */

/*-------------------------------------------------------------------------*/

/*  Open_Tcvr_Serial()
 *
 *  Opens Tcvr's Serial Port device, returns the file
 *  descriptor tcvr_serial_fd on success or exits on error
 */

  gboolean
Open_Tcvr_Serial( void )
{
  struct termios new_options; /* New serial port options */
  struct flock lockinfo;      /* File lock information   */
  int error = 0;
  char test[5];

  /* Abort if serial already open */
  if( isFlagSet(CAT_SETUP) ) return( TRUE );

  /* Open Serial device, exit on error */
  serial_fd = open( rc_data.cat_serial, O_RDWR | O_NOCTTY );
  if( serial_fd < 0 )
  {
    char mesg[MESG_SIZE];
    perror( rc_data.cat_serial );
    snprintf( mesg, sizeof(mesg),
        _("Failed to open %s\n"\
          "Transceiver CAT not setup"),
        rc_data.cat_serial );
    Error_Dialog( mesg );
    serial_fd = 0;
    return( FALSE );
  }

  /* Attempt to lock entire Serial port device file */
  lockinfo.l_type   = F_WRLCK;
  lockinfo.l_whence = SEEK_SET;
  lockinfo.l_start  = 0;
  lockinfo.l_len    = 0;

  /* If Serial device is already locked, abort */
  error = fcntl( serial_fd, F_SETLK, &lockinfo );
  if( error == -1 )
  {
    char mesg[MESG_SIZE];
    error = fcntl( serial_fd, F_GETLK, &lockinfo );
    if( error != -1 )
    {
      snprintf( mesg, sizeof(mesg),
          _("Failed to lock %s\n"\
            "Device locked by pid %d\n"\
            "Transceiver CAT not setup"),
          rc_data.cat_serial, lockinfo.l_pid );
      Error_Dialog( mesg );
    }
    close( serial_fd );
    serial_fd = 0;
    return( FALSE );
  }

  /* Save the current serial port options */
  error |= tcgetattr( serial_fd, &tcvr_serial_old_options );

  /* Read the current serial port options */
  error |= tcgetattr( serial_fd, &new_options );

  /* Set the i/o baud rates */
  switch( rc_data.receiver_type )
  {
    case FT847:
      error |= cfsetspeed( &new_options, B57600 );
      break;

    case FT857: case K2: case K3:
      error |= cfsetspeed( &new_options, B38400 );
      break;
  }

  /* Set options for 'raw' I/O mode */
  cfmakeraw( &new_options );

  /* Setup read() timeout to .5 sec */
  new_options.c_cc[ VMIN ]  = 0;
  new_options.c_cc[ VTIME ] = 5;

  /* Setup the new options for the port */
  error |= tcsetattr( serial_fd, TCSAFLUSH, &new_options );
  if( error ) return( FALSE );

  /* Enable CAT by testing comms with transceiver */
  Set_Flag( TCVR_SERIAL_TEST );
  switch( rc_data.receiver_type )
  {
    case FT847:
      if( !Write_Tcvr_Serial(FT847_CAT_ON, sizeof(FT847_CAT_ON)) ||
          !Write_Tcvr_Serial(FT847_MAIN_VFO, sizeof(FT847_MAIN_VFO)) ||
          !Read_Tcvr_Serial(test, 5) )
      {
        fprintf( stderr, "xdemorse: failed to enable FT847 CAT\n" );
        Write_Tcvr_Serial( FT847_CAT_OFF, sizeof(FT847_CAT_OFF) );
        Cancel_CAT();
        return( FALSE );
      }
      Write_Tcvr_Serial( FT847_MODE_CWNR, sizeof(FT847_MODE_CWNR) );
      break;

    case FT857:
      if( !Write_Tcvr_Serial(FT847_MAIN_VFO, sizeof(FT847_MAIN_VFO)) ||
          !Read_Tcvr_Serial(test, 5) )
      {
        fprintf( stderr, "xdemorse: failed to enable FT857 CAT\n" );
        Cancel_CAT();
        return( FALSE );
      }
      Write_Tcvr_Serial( FT847_MODE_CWR, sizeof(FT847_MODE_CWR) );
      break;

    case K2:
      /* Get K2 VFO A as a test of serial port */
      if( !Write_Tcvr_Serial(K3_VFOA_G, sizeof(K3_VFOA_G)-1) ||
          !Read_Tcvr_Serial(K3_VFOA_S, sizeof(K3_VFOA_S)-1) )
      {
        fprintf( stderr, "xdemorse: failed to enable K2 CAT\n" );
        Cancel_CAT();
        return( FALSE );
      }
      Write_Tcvr_Serial( K3_MODE_CWR, sizeof(K3_MODE_CWR)-1 );
      break;

    case K3:
      /* Set K3 to mode K31 as a test of serial port */
      Strlcpy( K3_K31_S, "K31;", sizeof(K3_K31_S) );
      if( !Write_Tcvr_Serial(K3_K31_S, sizeof(K3_K31_S)-1) )
      {
        fprintf( stderr, "xdemorse: failed to enable K3 CAT\n" );
        Cancel_CAT();
        return( FALSE );
      }
      if( !Write_Tcvr_Serial(K3_K31_G, sizeof(K3_K31_G)-1) ||
          !Read_Tcvr_Serial(K3_K31_S, sizeof(K3_K31_S)-1)  ||
          (strncmp(K3_K31_S, "K31;", sizeof(K3_K31_S)) != 0) )
      {
        fprintf( stderr, "xdemorse: failed to enable K3 CAT\n" );
        Cancel_CAT();
        return( FALSE );
      }
      Write_Tcvr_Serial( K3_MODE_CWR, sizeof(K3_MODE_CWR)-1 );
      break;

  } /* switch( rc_data.receiver_type ) */

  Clear_Flag( TCVR_SERIAL_TEST );
  Set_Flag( CAT_SETUP );

  return( TRUE );
} /* Open_Tcvr_Serial() */

/*-------------------------------------------------------------------------*/

/*  Close_Tcvr_Serial()
 *
 *  Restore old options and close the Serial port
 */

  void
Close_Tcvr_Serial( void )
{
  if( isFlagSet(CAT_SETUP) )
  {
    /* Disable CAT for FT847 */
    if( rc_data.receiver_type == FT847 )
      Write_Tcvr_Serial( FT847_CAT_OFF, sizeof(FT847_CAT_OFF) );

    /* Restore serial port and close */
    if( serial_fd > 0 )
    {
      tcsetattr( serial_fd, TCSANOW, &tcvr_serial_old_options );
      close( serial_fd );
      serial_fd = 0;
    }
  }

  Clear_Flag( CAT_SETUP );
} /* End of Close_Tcvr_Serial() */

/*-------------------------------------------------------------------------*/

/* Tune_Tcvr()
 *
 * Tunes the transceiver to the frequency of the strongest
 * signal near a mouse click in the waterfall window
 */

  gboolean
Tune_Tcvr( double x )
{
  int
    from, to,   /* Range to scan for a max bin value  */
    bin_max,    /* Max bin value found in this range  */
    max_idx,    /* ifft idx at which the max is found */
    ifft_idx,   /* Idx used to search ifft bin values */
    audio_freq; /* Audio frequency in waterfal window */

  /* Abort if serial already open */
  if( isFlagClear(CAT_SETUP) ) return( TRUE );

  /* Calculate ifft index corresponding to pointer x
   * in waterfall window. This is +1 over the pointer
   * position since the first FFT bin is not used */
  ifft_idx = (int)(x + 1.5);

  /* Look above and below click point for max bin val */
  from = ifft_idx - CLICK_TUNE_RANGE;
  if( from < 0 ) from = 0;
  to = ifft_idx + CLICK_TUNE_RANGE;
  if( to >= wfall_width ) to = wfall_width - 1;

  /* Find max bin value around click point */
  bin_max = 0;
  max_idx = ifft_idx;
  for( ifft_idx = from; ifft_idx < to; ifft_idx++ )
    if( bin_max < bin_ave[ifft_idx] )
    {
      bin_max = bin_ave[ifft_idx];
      max_idx = ifft_idx;
    }

  /* Audio frequency corresponding to ifft index. Since the
   * waterfall width has to be an odd number so that a center
   * line may be present, and since the FFT has to be a power
   * of 2 (e.g. even), the first (DC) output bin of the FFT is
   * not used and hence max_idx has to be increased by 1 and
   * the waterfall width also increased by one to correspond
   * with the FFT width. Only this way the audio frequency that
   * corresponds to FFT index can be calculated accurately. */
  audio_freq = ( 2 * (max_idx + 1) * rc_data.tone_freq ) / ( wfall_width + 1 );

  if( rc_data.receiver_type != PERSEUS )
  {
    /* Read current Rx frequency */
    rc_data.receiver_freq = 0;
    if( !Read_Rx_Freq(&rc_data.receiver_freq) )
      return( FALSE );
  }

  /* Add frequency correction */
  rc_data.receiver_freq += audio_freq - rc_data.tone_freq;
  gtk_spin_button_set_value( frequency, (gdouble)rc_data.receiver_freq );

  return( TRUE );
} /* Tune_Tcvr() */

/*-------------------------------------------------------------------------*/

